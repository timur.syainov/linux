#!/bin/bash
# hardware space(GB)


#***************Homework************************************************************
#В дополнении посчитать вывод Size Avail в K G +
#Реализовать вывод ошибки в случае некорректного ввода. Чему равняется $1, если он в case не KMG
#Вывести значение в процентом соотношени(%10+1).Подсчитать общую сумму. +

df -h

#*************************** colors ************************************************
Yellow='\e[93m'
Reset='\e[39m'
Magnet='\e[35m'
Gren='\e[32m'
Red='\e[31m'

#*********************** Passing parameters during script execution *********************************************
while [ -n "$1" ];
 do
    case "$1" in
      -t) echo "use temporary disk as input"
          Coef='^tmpfs' ;;
      -p) echo "use physical disk as input"
          Coef='^/dev' ;;
      -k) type='k';;
          echo "Displaying values in kilobyte"
      -m) type='m';;
          echo "Displaying values in megabytes"
      -g) type='g';;
          echo "Displaying values in gigabyte"
      *) echo "$1 the options entered does not match" ;;
    esac
    shift
 done 

#*************************** main *************************************************
Avail_space_array=$(df | grep $Coef | tr -s ' ' | cut -d ' '  -f 4)
Size_space_array=$(df | grep $Coef | tr -s ' ' | cut -d ' '  -f 2)
Use_space_array=$(df | grep $Coef | tr -s ' ' | cut -d ' ' -f 5 | tr % ' ') 


#******************* Avaid ********************************************************
array=($Avail_space_array)
for i in ${array[@]};
  do
    let "Sum_Avail+=i"
  done

#****************************** Size **********************************************
array=($Size_space_array)
for j in ${array[@]};
  do
    let "Sum_Size+=j"
  done

#******************************* IF ***********************************************
if [[ "$type" == "k" ]]; then
  Avail_Kb=$(echo "scale=3; $Sum_Avail" | bc)
  echo -e  "$Red Avail_space: $Avail_Kb"Kb" $Reset"

  Size_Kb=$(echo "scale=3; $Sum_Size" | bc)
  echo -e  "$Red Size_space: $Size_Kb"Kb" $Reset"
elif [[ "$type" == "m" ]]; then
  Avail_Mb=$(echo "scale=3; $Sum_Avail/1024" | bc)
  echo -e  "$Yellow Avail_space: $Avail_Mb"Mb" $Reset"

  Size_Mb=$(echo "scale=3; $Sum_Size/1024" | bc)
  echo -e  "$Yellow Size_space: $Size_Mb"Mb" $Reset"
elif [[ "$type" == "g" ]]; then
  Avail_Gb=$(echo "scale=3; $Sum_Avail/1024/1024" | bc)
  echo -e  "$Gren Avail_space: $Avail_Gb"Gb" $Reset"

  Size_Gb=$(echo "scale=3; $Sum_Size/1024/1024" | bc)
  echo -e  "$Gren Size_space: $Size_Gb"Gb" $Reset"
fi

#****************************** Use %****************************
array=($Use_space_array)
for g in ${array[@]};
 do
  let "Sum_Use+=g"
 done

#****************************** Output values Use in SUMM % **********************
Use=$(echo "scale=3; $Sum_Use" | bc)
echo -e  "$Magnet Use_space_ALL: $Use"%" $Reset" 