
#!/bin/bash

echo "Cкрипт, который подсчитывает индекс массы тела по формуле"

	while getopts ":w:h:" OPTION; do

case $OPTION in

w) WEIGHT="$OPTARG";;
h) HEIGHT="$OPTARG";;

esac

done

echo $WEIGHT
echo $HEIGHT

C=$(echo "scale=3; $WEIGHT/($HEIGHT^2)*703" | bc -l)
echo "BIM: $C"

function in_range() {
  min=$1
  val=$2
  max=$3

  echo "checking if $val is in ($min;$max]"
  #echo "$val > $min"
  #echo $(echo "$C > $min" |bc)
  #echo "$C <= $max"
  #echo $(echo "$C <= $max" |bc)
  #echo "Result"
  # echo [ $(echo "$C > $min" |bc) ] && [ $(echo "$C <= $max" |bc) ]
  if [[ $(echo "$C > $min" |bc) == 1  && $(echo "$C <= $max" |bc) == "1" ]]; then
    echo "between"
  else
    echo "not between"
  fi
}

in_range 0 "$C" 18
in_range 18 "$C" 24
in_range 25 "$C" 29
in_range 30 "$C" 1000

if [ $(echo "$C <= 18" |bc) -eq 1 ]; then
                echo "Weight class: underweight (<=18)"

elif [[ $(echo "$C > 18" |bc) -eq 1 && $(echo "$C <= 24" |bc) -eq 1 ]]; then
                echo  "Weight class: normal (18-24)";

elif [[ $(echo "$C >= 25" |bc) -eq 1 && $(echo "$C <= 29" |bc) -eq 1 ]]; then
                echo  "Weight class: overweight (25-29)";

elif [ $(echo "$C >= 30" |bc) -eq 1 ]; then
                echo "Weight class: Obese (>=30)";

else 
                echo "Unknown class"
fi




